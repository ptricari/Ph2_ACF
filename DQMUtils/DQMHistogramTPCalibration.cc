/*!
        \file                DQMHistogramTPCalibration.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Tricarico Pietro, Lorenzo Uplegger
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
 */

#include "../DQMUtils/DQMHistogramTPCalibration.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/EmptyContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ContainerFactory.h"
#include "../RootUtils/TH1FContainer.h"
#include "../RootUtils/TH2FContainer.h"
#include "../Utils/Container.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "../Utils/GenericVariableArray.h"

//======================================================================================================================================================================================================//
// Constractor and Destructor of TPCalibration(Test Pulse Calibration) DQM(Data Quality Monitoring) Histograms                                                                                          //                                               
//======================================================================================================================================================================================================//

DQMHistogramTPCalibration::DQMHistogramTPCalibration ()
{
}
//=====================================================================================================================================================================================================//

DQMHistogramTPCalibration::~DQMHistogramTPCalibration ()
{
}

//======================================================================================================================================================================================================//
// Book all Histograms of TPCalibration Calibration                                                                                                                                                     //                                               
//======================================================================================================================================================================================================//

void DQMHistogramTPCalibration::book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap)
{
    ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

    int fStartAmp = 165; /// ??
    int fEndAmp = 255;  /// ??
    int fStepsize = 10; /// ??
    
    // Book Chip 1D-plot Histograms  x = Channel# ; y = Gain (Amp) ; 
    HistContainer<TH1F> hGainAmp("GainAmp","Gain (Amp)",NCHANNELS, -0.5, NCHANNELS - 0.5);
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorChannelGainAmpHistograms,hGainAmp);

    // Book Chip 1D-plot Histograms  x = Channel# ; y = Gain (Electron) ; 
    HistContainer<TH1F> hGainElectron("GainElectron","Gain (Electron)",NCHANNELS, -0.5, NCHANNELS - 0.5);
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorChannelGainElectronHistograms,hGainElectron);

    float cBinWidth = 0.005;
    float cMin = 0.9;
    float cMax = 1.1;

    // Book Chip 1D-plot Histograms  x =  ; y = Gain (Amp/VCth) ; 
    HistContainer<TH1F> hGainAmpVCth("GainAmp","Gain (Amp/VCth)",(cMax-cMin)/cBinWidth, cMin, cMax);
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorChannelGainAmpVCthHistograms,hGainAmpVCth);

    // Book Chip 1D-plot Histograms  x =  ; y = Gain (electrons/VCth) ; 
    HistContainer<TH1F> hGainElectronVCth("GainElectron","Gain (electrons/VCth)",(cMax-cMin)/cBinWidth, ConvertAmpToElectrons(cMin, false),ConvertAmpToElectrons(cMax, false));
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorChannelGainElectronVCthHistograms,hGainElectronVCth);

    // Book Chip 2D-plot Histograms  x = Channel# ; y = Threshold# ; z = # Test Pulse Amplitude (ADC) ;
    HistContainer<TH2F> hChanCorr("testPulseAmplitude","Test Pulse Amplitude;  Channel # ; Threshold [Vcth]; # Test Pulse Amplitude (ADC)",NCHANNELS, -0.5, NCHANNELS - 0.5,10,-0.5,9.5);
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorTestPulseAmplitudeHistograms,hChanCorr);

}

//======================================================================================================================================================================================================//
// TPCalibration fill, process and reset the Histograms                                                                                                                                                 //                                               
//======================================================================================================================================================================================================//

bool DQMHistogramTPCalibration::fill(std::vector<char>& dataBuffer)
{
        return false;
}

//=====================================================================================================================================================================================================//
void DQMHistogramTPCalibration::process()
{
    for(auto board : fDetectorChannelGainAmpHistograms)
    {
        for(auto module: *board)
        {
            for(auto chip: *module)
            {   
                TCanvas *chipGainCanvas = new TCanvas(Form("chipGainCanvas%d",chip->getId()),Form("Chip %d Gain Canvas",chip->getId()),10, 0, 1000, 600 );
                chipGainCanvas ->Divide(2,2);

                // Draw GainAmp Histograms x = Channel# ; y = Gain (Amp) ; 
                chipGainCanvas->cd(1);
                TH1F *chipGainAmpHistograms = fDetectorChannelGainAmpHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                chipGainAmpHistograms->DrawCopy();

                // Draw GainElectron Histograms x = Channel# ; y = Gain (Electrons) ; 
                chipGainCanvas->cd(2);
                TH1F *chipGainElectronsHistograms = fDetectorChannelGainElectronHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                chipGainElectronsHistograms->DrawCopy();

                // Draw GainAmpVCth Histograms x =  ; y = Gain (Amp/VCth) ;
                chipGainCanvas->cd(3);
                TH1F *chipGainAmpVCthHistograms = fDetectorChannelGainAmpVCthHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                chipGainAmpVCthHistograms->DrawCopy();

                // Draw GainElectronsVCth Histograms x =  ; y = Gain (electrons/VCth) ; 
                chipGainCanvas->cd(4);
                TH1F *chipGainElectronsVCthHistograms = fDetectorChannelGainElectronVCthHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                chipGainElectronsVCthHistograms->DrawCopy();


                TCanvas *chipSignalScanCanvas1 = new TCanvas(Form("tp%d",chip->getId()),Form("tp %d  Canvas",chip->getId()),10, 0, 1000, 600 );

                // Draw TestPulseAmplitude Histograms x = Channel# ; y = Threshold# ; z = # Test Pulse Amplitude (ADC) ;
                TH1F *chipTestPulseAmplitudeHistograms = fDetectorTestPulseAmplitudeHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                chipTestPulseAmplitudeHistograms->DrawCopy("colz");

            }

        }
    }

}

//=====================================================================================================================================================================================================//

void DQMHistogramTPCalibration::reset(void)
{

}

//======================================================================================================================================================================================================//
// Fill the single plots of TPCalibration                                                                                                                                                               //                                               
//======================================================================================================================================================================================================//

void DQMHistogramTPCalibration::fillChipGainPlots(uint16_t Channel, DetectorDataContainer &theGainContainer)
{
    for(auto board: theGainContainer)
    {
        for(auto module: *board)
        {
            for(auto chip: *module)
            {
                // Fill GainAmp Histograms x = Channel# ; y = Gain (Amp) ; 
                TH1F *chipGainAmpHistograms = fDetectorChannelGainAmpHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                float gainAmp = theGainContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[0];
                chipGainAmpHistograms->Fill(Channel,gainAmp);
                chipGainAmpHistograms->SetBinError(chipGainAmpHistograms->FindBin(Channel),sqrt(gainAmp));

                // Fill GainElectron Histograms x = Channel# ; y = Gain (Electrons) ; 
                TH1F *chipGainElectronsHistograms = fDetectorChannelGainElectronHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                float gainElectrons = theGainContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[0];
                chipGainElectronsHistograms->Fill(Channel,gainElectrons);
                chipGainElectronsHistograms->SetBinError(chipGainElectronsHistograms->FindBin(Channel),sqrt(gainElectrons));

                // Fill GainAmpVCth Histograms x =  ; y = Gain (Amp/VCth) ;
                TH1F *chipGainAmpVCthHistograms = fDetectorChannelGainAmpVCthHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                float gainAmpVCth = theGainContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[0];
                chipGainAmpVCthHistograms->Fill(Channel,gainAmpVCth);
                chipGainAmpVCthHistograms->SetBinError(chipGainAmpVCthHistograms->FindBin(Channel),sqrt(gainAmpVCth));

                // Fill GainElectronsVCth Histograms x =  ; y = Gain (electrons/VCth) ;
                TH1F *chipGainElectronsVCthHistograms = fDetectorChannelGainElectronVCthHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                float gainElectronsVCth = theGainContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[0];
                chipGainElectronsVCthHistograms->Fill(Channel,gainElectronsVCth);
                chipGainElectronsVCthHistograms->SetBinError(chipGainElectronsVCthHistograms->FindBin(Channel),sqrt(gainElectronsVCth));

            }
        }
    }
}
//=====================================================================================================================================================================================================//

void DQMHistogramTPCalibration::fillChipTestPulseAmplitudePlots(uint16_t Threshold, DetectorDataContainer &theTestPulseAmplitudeContainer)
{
    for(auto board: theTestPulseAmplitudeContainer)
    {
        for(auto module: *board)
        {
            for(auto chip: *module)
            {
                //Fill TestPulseAmplitude Histograms x = Channel# ; y = Threshold# ; z = # Test Pulse Amplitude (ADC) ;
                TH2F *testPulseAmplitudeHistograms = theTestPulseAmplitudeContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;
            
                for ( uint32_t cId = 0; cId < NCHANNELS; cId++ )
                {
                    float AmplitudeADC = chip->getChannel<float>(cId) ;
                    testPulseAmplitudeHistograms ->SetBinContent(cId ,Threshold, AmplitudeADC);
                }

            }
        }
    }
}
//=====================================================================================================================================================================================================//

float DQMHistogramTPCalibration::ConvertAmpToElectrons(float pTPAmp, bool pOffset=true)
{
  // Test pulse step resolution (of 0.086fC = 537electrons)
  // taken from the CBC3 manual (should be the same for CBC2)
  // 255 equals no test pulse with decreasing values the injected charge
  // increases
  if(!pOffset)
  {
    return pTPAmp*537.;
  }
  return -(pTPAmp - 255)*537.;
}