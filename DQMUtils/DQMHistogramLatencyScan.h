/*!
        \file                DQMHistogramLatencyScan.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger,Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
*/

#ifndef __DQMHISTOGRAMLATENCYSCAN_H__
#define __DQMHISTOGRAMLATENCYSCAN_H__
#include "../DQMUtils/DQMHistogramBase.h"
#include "../Utils/Container.h"
#include "../Utils/DataContainer.h"

class TFile;

/*!
 * \class DQMHistogramLatencyScan
 * \brief Class for PedeNoise monitoring histograms
 */
class DQMHistogramLatencyScan : public DQMHistogramBase
{

  public:
    /*!
     * constructor
     */
    DQMHistogramLatencyScan ();

    /*!
     * destructor
     */
    ~DQMHistogramLatencyScan();

    /*!
     * Book histograms
     */
    void book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill (std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process () override;

    /*!
     * Reset histogram
     */
    void reset(void) override;
    //virtual void summarizeHistos();

    
    void fillTriggerTDCPlots(size_t tdcValue, DetectorDataContainer &cointener);
    void fillLatencyPlots( uint16_t latencyValue, DetectorDataContainer &theLatencyCointainer);
    void fillStubLatencyPlots(uint16_t latencyValue, DetectorDataContainer &stubLatency);
    void fillLatency2DPlots(uint16_t Latency, uint16_t StubLatency, DetectorDataContainer &Hitslatency2D);
 
  private:
    DetectorDataContainer fDetectorData;
    
    DetectorDataContainer fDetectortriggerTDCHistograms;
    DetectorDataContainer fDetectorLatencyHistograms;
    DetectorDataContainer fDetectorStubLatencyHistograms;
    DetectorDataContainer fDetectorLatencyScan2DHistograms;


};
#endif
