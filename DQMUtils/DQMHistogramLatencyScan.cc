/*!
        \file                DQMHistogramLatencyScan.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch, pietro.matteo.tricarico@cern.ch 
 */

#include "../DQMUtils/DQMHistogramLatencyScan.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Utilities.h"
#include "../Utils/EmptyContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ContainerFactory.h"
#include "../RootUtils/TH1FContainer.h"
#include "../RootUtils/TH2FContainer.h"
#include "../Utils/Container.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"

//=================================================================================================================================================================================================================//
// Constractor and Destructor of LatencyScan DQM(Data Quality Monitoring) Histograms                                                                                                                               //                                               
//=================================================================================================================================================================================================================//

DQMHistogramLatencyScan::DQMHistogramLatencyScan ()
{
}
//================================================================================================================================================================================================================//

DQMHistogramLatencyScan::~DQMHistogramLatencyScan ()
{

}

//=================================================================================================================================================================================================================//
// Book all Histograms of LatencyScan Calibration                                                                                                                                                                  //                                               
//=================================================================================================================================================================================================================//

//========================================================================================================================
void DQMHistogramLatencyScan::book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap)
{
    ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

    uint16_t latencyStart = pSettingsMap["StartLatency"];
    uint16_t latencyRange = pSettingsMap["LatencyRange"];
   
    const uint8_t TDCBins = 8;
    
    //Trigger TDC (Time to digital converter)
    // Book Board 1D-plot Histogram with the number of Hits per Trigger TDC.
    HistContainer<TH1F> hTriggerTDC("boardTriggerTDC","Trigger TDC;   Trigger TDC; # of Hits",TDCBins, -0.5, TDCBins-0.5);
    RootContainerFactory::bookBoardHistograms(theOutputFile,theDetectorStructure,fDetectortriggerTDCHistograms,hTriggerTDC);

    //Module Latency
    // Book Module 1D-plot Histogram with the number of Hits per Latency.
    HistContainer<TH1F> hLatency("moduleLatencyScan","Latency Scan;  Trigger Latency; # of Hits", (latencyRange) , latencyStart - 0.5,  latencyStart + (latencyRange ) - 0.5 );
    RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorLatencyHistograms,hLatency);
    
    //Module Stub Latency
    // Book Module 1D-plot Histogram with the number of Stubs per Latency.
    HistContainer<TH1F> hStub("moduleStubLatencyScan","Stub Latency;  Stub Latency; # of Stubs ", latencyRange, latencyStart, latencyStart + latencyRange);
    RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorStubLatencyHistograms,hStub);
    
    //Module Latency 2D (L1 Latency  Stub Latency) 
    // Book Module 2D-plot Histogram with Stub Latency on the x-axis, L1 Latency on y-axis,  number of Events w/ no Hits and no Stubs on z-axis.
    HistContainer<TH2D> hLatencyScan2D( "moduleLatencyScan2D","Latency Scan 2DL;   Stub Latency; L1 Latency; # of Events w/ no Hits and no Stubs"
                                                                        , latencyRange  , latencyStart - 0.5 ,  latencyStart + (latencyRange ) - 0.5 
                                                                        , latencyRange  , latencyStart - 0.5 ,  latencyStart + (latencyRange ) - 0.5   );
    RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorLatencyScan2DHistograms,hLatencyScan2D);
}

//=================================================================================================================================================================================================================//
// LatencyScan Calibration fill, process and reset the Histograms                                                                                                                                                  //                                               
//=================================================================================================================================================================================================================//

bool DQMHistogramLatencyScan::fill(std::vector<char>& dataBuffer)
{
        return false;
}
//================================================================================================================================================================================================================//

void DQMHistogramLatencyScan::process()
{
    for(auto board : fDetectorLatencyHistograms)
    {   
        for(auto module: *board)
        {
            TCanvas *ModulelatencyCanvas = new TCanvas(Form("ModuleLatencyScanCanvas%d",module->getId()),Form("Module %d Latency Scan Canvas",module->getId()),10, 0, 1300, 800 );
            ModulelatencyCanvas ->Divide(2,2);

            // Draw Latency Histogram  x = Channel# ; y = Threshold# ; z = # of Hits# ;
            ModulelatencyCanvas->cd(1);
            TH1I* latencyHistogram      = module->getSummary<HistContainer<TH1I>>().fTheHistogram;
            latencyHistogram->SetFillColor ( 4 );
            latencyHistogram->SetFillStyle ( 3001 );
            latencyHistogram->DrawCopy();

            ModulelatencyCanvas->cd(2);
            TH1F* stubLatencyHistogram = fDetectorStubLatencyHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<TH1FContainer>().fTheHistogram;
            stubLatencyHistogram->SetMarkerStyle ( 2 );
            stubLatencyHistogram->DrawCopy();

            // analyze the Histograms
            LOG (INFO) << "Identified the Latency with the maximum number of Stubs at: " ;
            uint8_t cStubLatency =  static_cast<uint8_t> ( stubLatencyHistogram->GetMaximumBin() - 1 );
            LOG (INFO) << "Stub Latency FE " << +module->getId()  << ": " << +cStubLatency << " clock cycles!" ;

            ModulelatencyCanvas->cd(3);
            TH2D* latency2DHistogram   = fDetectorLatencyScan2DHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2D>>().fTheHistogram;
            latency2DHistogram->DrawCopy("colz");


            // now display a message to the user to let them know what the optimal latencies are for each Module (FE, Front end)
            std::pair<uint8_t, uint16_t> cOptimalLatencies;
            cOptimalLatencies.first = 0 ;
            cOptimalLatencies.second = 0; 
            int cMaxNEvents_wBoth = 0 ; 

            for( int cBinX = 1 ; cBinX < latency2DHistogram->GetXaxis()->GetNbins() ; cBinX ++ )
            {
                for( int cBinY = 1 ; cBinY < latency2DHistogram->GetYaxis()->GetNbins() ; cBinY ++ )
                {
                    int cBinContent = latency2DHistogram->GetBinContent( cBinX , cBinY ); 
                    if( cBinContent >= cMaxNEvents_wBoth )
                    {
                        cOptimalLatencies.first = (uint8_t)latency2DHistogram->GetXaxis()->GetBinCenter(cBinX);
                        cOptimalLatencies.second = (uint16_t)latency2DHistogram->GetYaxis()->GetBinCenter(cBinY);
                        cMaxNEvents_wBoth = cBinContent;
                    }
                }
            }
            LOG (INFO) << BOLDRED << "************************************************************************************" << RESET ;
            LOG (INFO) << BOLDRED << "For FE" << +module->getId() << " found optimal latencies to be : " << RESET ; 
            LOG (INFO) << BOLDRED << "........ Stub Latency of " << +cOptimalLatencies.first << " and a Trigger Latency of " << +cOptimalLatencies.second << RESET; 
            LOG (INFO) << BOLDRED << "************************************************************************************" << RESET ;

        }
        
        TCanvas *boardlatencyCanvas = new TCanvas(Form("boardLatencyScanCanvas%d",board->getId()),Form("Board %d Latency Scan Canvas",board->getId()),10, 0, 900, 500 );
        boardlatencyCanvas->cd(1);
        TH1F *tdcHistogram = fDetectortriggerTDCHistograms.at(board->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
        tdcHistogram->SetFillColor ( 4 );
        tdcHistogram->SetFillStyle ( 3001 );
        tdcHistogram->DrawCopy();
    }

}
//================================================================================================================================================================================================================//

void DQMHistogramLatencyScan::reset(void)
{

}

//=================================================================================================================================================================================================================//
// Fill plots of LatencyScan                                                                                                                                                                                       //                                               
//=================================================================================================================================================================================================================//

void DQMHistogramLatencyScan::fillTriggerTDCPlots(size_t tdcValue, DetectorDataContainer &theTdcValueCointener)
{
    for(auto board : theTdcValueCointener)
    {
        TH1F *boardtriggerTDCHiHistogram = fDetectortriggerTDCHistograms.at(board->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
        uint16_t NHits = theTdcValueCointener.at(board->getIndex())->getSummary<uint16_t>();
        boardtriggerTDCHiHistogram->SetBinContent(tdcValue+1, NHits);
        //boardtriggerTDCHiHistogram->Fill(tdcValue,NHits);
    }
}
//================================================================================================================================================================================================================//

void DQMHistogramLatencyScan::fillLatencyPlots(uint16_t cLat, DetectorDataContainer &theLatencyCointainer)
{
    for(auto board : theLatencyCointainer)
    {
        for(auto module: *board)
        {
            TH1I *moduleLatencytHistogram = fDetectorLatencyHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;

            uint16_t cHitCounter = theLatencyCointainer.at(board->getIndex())->at(module->getIndex())->getSummary<uint16_t>();
            
            moduleLatencytHistogram->SetBinContent(cLat+1, cHitCounter);
            //moduleLatencytHistogram->Fill(cLat,cHitCounter);
            moduleLatencytHistogram->SetBinError(moduleLatencytHistogram->FindBin(cLat),sqrt(cHitCounter));
        }
    }
}
//================================================================================================================================================================================================================//

void DQMHistogramLatencyScan::fillStubLatencyPlots(uint16_t cLat, DetectorDataContainer &theStubLatencyCointainer)
{
    for(auto board : theStubLatencyCointainer)
    {
        for(auto module: *board)
        {
            TH1I *moduleStubLatenctHistogram = fDetectorStubLatencyHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;

            uint16_t cHitCounter = theStubLatencyCointainer.at(board->getIndex())->at(module->getIndex())->getSummary<uint16_t>();
            //moduleStubLatenctHistogram->SetBinContent(cLat+1, cHitCounter);
            moduleStubLatenctHistogram->Fill(cLat,cHitCounter);
            moduleStubLatenctHistogram->SetBinError(moduleStubLatenctHistogram->FindBin(cLat),sqrt(cHitCounter));
            
        }
    }

}
//================================================================================================================================================================================================================//

void DQMHistogramLatencyScan::fillLatency2DPlots(uint16_t cLat, uint16_t cStubLatency, DetectorDataContainer &theScanLatency2DContainer)
{
    for(auto board : theScanLatency2DContainer)
    {
        for(auto module: *board)
        {
            TH2D *modules2DLatencytHistogram = fDetectorLatencyScan2DHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2D>>().fTheHistogram;
            uint16_t cHitCounter = theScanLatency2DContainer.at(board->getIndex())->at(module->getIndex())->getSummary<uint16_t>();
            modules2DLatencytHistogram->Fill(cStubLatency,cLat,cHitCounter);
            modules2DLatencytHistogram->SetBinError(modules2DLatencytHistogram->FindBin(cLat),sqrt(cHitCounter));
        }
    }


}
//================================================================================================================================================================================================================//
