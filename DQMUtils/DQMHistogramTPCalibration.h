/*!
        \file                DQMHistogramTPCalibration.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
*/

#ifndef __DQMHISTOGRAMTPCALIBRATION_H__
#define __DQMHISTOGRAMTPCALIBRATION_H__
#include "../DQMUtils/DQMHistogramBase.h"
#include "../Utils/Container.h"
#include "../Utils/DataContainer.h"

class TFile;

/*!
 * \class DQMHistogramTPCalibration
 * \brief Class for PedeNoise monitoring histograms
 */
class DQMHistogramTPCalibration : public DQMHistogramBase
{

  public:
    /*!
     * constructor
     */
    DQMHistogramTPCalibration ();

    /*!
     * destructor
     */
    ~DQMHistogramTPCalibration();

    /*!
     * Book histograms
     */
    void book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill (std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process () override;

    /*!
     * Reset histogram
     */
    void reset(void) override;
    //virtual void summarizeHistos();

    void fillChipGainPlots(uint16_t Channel, DetectorDataContainer &theGainContainer);
    void fillChipTestPulseAmplitudePlots(uint16_t Threshold, DetectorDataContainer &theTestPulseAmplitudeContainer);

    float ConvertAmpToElectrons(float pTPAmp, bool pOffset);

    


  private:
    DetectorDataContainer fDetectorData;

    DetectorDataContainer fDetectorChannelGainAmpHistograms;
    DetectorDataContainer fDetectorChannelGainElectronHistograms;

    DetectorDataContainer fDetectorChannelGainAmpVCthHistograms;
    DetectorDataContainer fDetectorChannelGainElectronVCthHistograms;

    DetectorDataContainer fDetectorTestPulseAmplitudeHistograms;

};
#endif
