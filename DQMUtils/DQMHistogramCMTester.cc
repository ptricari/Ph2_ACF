/*!
        \file                DQMHistogramCMTester.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
 */

#include "../DQMUtils/DQMHistogramCMTester.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Utilities.h"
#include "../Utils/Occupancy.h"
#include "../Utils/EmptyContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ContainerFactory.h"
#include "../RootUtils/TH1FContainer.h"
#include "../RootUtils/TH2FContainer.h"
#include "../Utils/Container.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TProfile2D.h"

//=================================================================================================================================================================================================================//
// Constractor and Destructor of CMTester(Common Mode noise studies) DQM(Data Quality Monitoring) Histograms                                                        //                                               
//=================================================================================================================================================================================================================//

DQMHistogramCMTester::DQMHistogramCMTester ()
{
}
//================================================================================================================================================================================================================//

DQMHistogramCMTester::~DQMHistogramCMTester ()
{

}

//=================================================================================================================================================================================================================//
// Book all Histograms of CMTester Calibration                                                                                                                      //                                               
//=================================================================================================================================================================================================================//

void DQMHistogramCMTester::book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap)
{
        ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

        // histogram for the number of hits
        HistContainer<TH1F> hNHits("NumberOfHits", "Number of Hits;    Hits; Count ", NCHANNELS + 1, -.5, NCHANNELS + 0.5 );
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorNHitsHistograms,hNHits);


        // 2D profile for the combined odccupancy
        HistContainer<TProfile2D> hCombinedOccupancy("CombinedOccupancy","Combined Occupancy;    Strip; Strip; Occupancy",NCHANNELS + 1, -0.5, NCHANNELS + 0.5, NCHANNELS + 1, -0.5, NCHANNELS + 0.5 );
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorCombinedOccupancyHistograms,hCombinedOccupancy);

        // 2D Profile for correlation coefficient
        HistContainer<TH2F> hCorrelationCoefficient("CorrelationCoefficient","Correlation;    Strip; Strip; Correlation coefficient",NCHANNELS + 1, -.5, NCHANNELS + 0.5, NCHANNELS + 1, -.5, NCHANNELS + 0.5 );
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorCorrelationCoefficientHistograms,hCorrelationCoefficient);

        // 1D projection of the combined odccupancy
        HistContainer<TProfile> hOccupancyProjection("ProjectionOfCombinedOccupancy","Projection of combined Occupancy;    NNeighbors; Probability", NCHANNELS + 1, -.5, NCHANNELS + 0.5 );
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorOccupancyProjectionHistograms,hOccupancyProjection);

        // 1D projection of the combined occupancy, but nearest neighbor calculated on both sides
        HistContainer<TProfile> hOccupancyProjectionPlusMinus("ProjectionOfCombinedOccupancyPlusAndMinusS "," Projection of combined Occupancy (+ and -);    NNeighbors (+-N); Probability", NCHANNELS + 1, -.5, NCHANNELS + 0.5);
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorOccupancyProjectionPlusMinusHistograms,hOccupancyProjectionPlusMinus);

        // 1D projection of the uncorrelated odccupancy
        HistContainer<TProfile> hUncorrOccupancyProjection("ProjectionOfUncorrelatedOccupancy ","Projection of uncorrelated Occupancy;   NNeighbors; Probability ",NCHANNELS + 1, -.5, NCHANNELS + 0.5);
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorUncorrOccupancyProjectionHistograms,hUncorrOccupancyProjection);


        // 1D projection of the correlation
        HistContainer<TProfile> hCorrelationProjection("ProjectionOfCorrelation ","Projection of Correlation;   NNeighbors; Correlation ",NCHANNELS + 1, -.5, NCHANNELS + 0.5);
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectororrelationProjectionHistograms,hCorrelationProjection);

        // 1D hit probability profile
        HistContainer<TProfile> hhitprob("hitProb","Hit Probability;   Strip; Probability", NCHANNELS + 1, -.5, NCHANNELS + 0.5);
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorhitprobHistograms,hhitprob);

        // dummy TF1* for fit & dummy TH1F* for 0CM
        HistContainer<TH1F> hNHitsFit(" hitsFit","hitProbFunction ?? ",0, 255, 4);
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorNHitsFitHistograms,hNHitsFit);

        //TH1F Noise hit distributtion
        HistContainer<TH1F> hNoiseHitDistribution("noiseHitDistributtion","Noise hit distributtion ", NCHANNELS + 1, -0.5, NCHANNELS + 0.5 );
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorNoiseHitDistributionHistograms,hNoiseHitDistribution);
        
        // PER MODULE PLOTS ===================================================================================================================================================//
        int NCbc = 8;

        // 2D profile for the combined odccupancy
        HistContainer<TH2F> hModuleCombinedOccupancy("ModuleCombinedOccupancy","Combined Occupancy;  Strip; Strip; Occupancy",NCbc * NCHANNELS + 1, -.5, NCbc * NCHANNELS + 0.5, NCbc * NCHANNELS + 1, -.5, NCbc * NCHANNELS + 0.5);
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorModuleCombinedOccupancyHistograms,hModuleCombinedOccupancy);

        // 2D Hist for correlation coefficient
        HistContainer<TH2F> hModuleCorrelation("ModuleCorrelationCoefficient ","Correlation;  Strip; Strip; Correlation coefficient ",NCbc * NCHANNELS + 1, -.5, NCbc * NCHANNELS + 0.5, NCbc * NCHANNELS + 1, -.5, NCbc * NCHANNELS + 0.5);
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorModuleCorrelationHistograms,hModuleCorrelation);

        // 1D projection of the combined odccupancy
        HistContainer<TH1F> hModuleOccupancyProjection("ModuleOccupancyProjection"," Projection of combined Occupanc;   NNeighbors; Probability",NCbc * NCHANNELS + 1, -.5, NCbc * NCHANNELS + 0.5 );
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorModuleOccupancyProjectionHistograms,hModuleOccupancyProjection);

        // 1D projection of the uncorrelated occupancy
        HistContainer<TH1F> hModuleUncorrOccupancyProjection("ModuleUncorrelatedOccupancyProjection","Projection of uncorrelated Occupancy;   NNeighbors; Probability ",NCbc * NCHANNELS + 1, -.5, NCbc * NCHANNELS + 0.5);
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorModuleUncorrOccupancyProjectionHistograms,hModuleUncorrOccupancyProjection);

        // 1D projection of the correlation
        HistContainer<TH1F> hModuleCorrelationProjection("ModuleCorrelationProjection","Projection of Correlation;  NNeighbors; Correlation",NCbc * NCHANNELS + 1, -.5, NCbc * NCHANNELS + 0.5);
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorModuleCorrelationProjectionHistograms,hModuleCorrelationProjection);

}

//===================================================================================================================================================================================================================//
// CMTester fill, process and reset the Histograms                                                                                                                                                                   //                                               
//===================================================================================================================================================================================================================//

bool DQMHistogramCMTester::fill(std::vector<char>& dataBuffer)
{
        return false;
}

//================================================================================================================================================================================================================//
void DQMHistogramCMTester::process()
{
        for(auto board : fDetectorNHitsHistograms)
        {   
                for(auto module: *board)
                {
                        for(auto chip: *module )
                        {
                                TCanvas *ModulelatencyCanvas = new TCanvas(Form("ModuleLatencyScanCanvas%d",module->getId()),Form("Module %d Latency Scan Canvas",module->getId()),10, 0, 1300, 800 );
                                ModulelatencyCanvas ->Divide(2,2);

                                ModulelatencyCanvas->cd(1);
                                TH1I* latencyHistogram = module->getSummary<HistContainer<TH1I>>().fTheHistogram;
                                latencyHistogram->DrawCopy();

                                ModulelatencyCanvas->cd(2);
                                TH1F* stubLatencyHistogram = fDetectorCombinedOccupancyHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<TH1FContainer>().fTheHistogram;
                                stubLatencyHistogram->DrawCopy();

                                ModulelatencyCanvas->cd(3);
                                TH2D* latency2DHistogram =fDetectorCorrelationCoefficientHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2D>>().fTheHistogram;
                                latency2DHistogram->DrawCopy("colz");
                        }

                        TCanvas *boardlatencyCanvas = new TCanvas(Form("boardLatencyScanCanvas%d",board->getId()),Form("Board %d Latency Scan Canvas",board->getId()),10, 0, 900, 500 );
                        boardlatencyCanvas->cd(1);
                        TH1F *tdcHistogram = fDetectorOccupancyProjectionHistograms.at(board->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                        tdcHistogram->GetXaxis()->SetTitle("Trigger TDC");
                        tdcHistogram->GetYaxis()->SetTitle("# of Hits");
                        tdcHistogram->SetFillColor ( 4 );
                        tdcHistogram->SetFillStyle ( 3001 );
                        tdcHistogram->DrawCopy();
                }
                

        }
        

}

//================================================================================================================================================================================================================//

void DQMHistogramCMTester::reset(void)
{

}

//=================================================================================================================================================================================================================//
// Fill the single plots of CMTester                                                                                                                                //                                               
//=================================================================================================================================================================================================================//



