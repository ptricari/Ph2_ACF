/*!
        \file                DQMHistogramCMTester.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
*/

#ifndef __DQMHISTOGRAMCMTESTER_H__
#define __DQMHISTOGRAMCMTESTER_H__
#include "../DQMUtils/DQMHistogramBase.h"
#include "../Utils/Container.h"
#include "../Utils/DataContainer.h"

class TFile;

/*!
 * \class DQMHistogramCMTester
 * \brief Class for CMTester monitoring histograms
 */
class DQMHistogramCMTester : public DQMHistogramBase
{

  public:
    /*!
     * constructor
     */
    DQMHistogramCMTester ();

    /*!
     * destructor
     */
    ~DQMHistogramCMTester();

    /*!
     * Book histograms
     */
    void book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill (std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process () override;

    /*!
     * Reset histogram
     */
    void reset(void) override;
    //virtual void summarizeHistos();


  private:
    DetectorDataContainer fDetectorData;

    DetectorDataContainer fDetectorNHitsHistograms;
    
    DetectorDataContainer fDetectorCombinedOccupancyHistograms;
    DetectorDataContainer fDetectorCorrelationCoefficientHistograms;
    DetectorDataContainer fDetectorOccupancyProjectionHistograms;
    DetectorDataContainer fDetectorOccupancyProjectionPlusMinusHistograms;
    DetectorDataContainer fDetectorUncorrOccupancyProjectionHistograms;

    DetectorDataContainer fDetectororrelationProjectionHistograms;

    DetectorDataContainer fDetectorhitprobHistograms;
    DetectorDataContainer fDetectorNHitsFitHistograms;
    DetectorDataContainer fDetectorNoiseHitDistributionHistograms;


    DetectorDataContainer fDetectorModuleCombinedOccupancyHistograms;
    DetectorDataContainer fDetectorModuleCorrelationHistograms;
    DetectorDataContainer fDetectorModuleOccupancyProjectionHistograms;
    DetectorDataContainer fDetectorModuleUncorrOccupancyProjectionHistograms;
    DetectorDataContainer fDetectorModuleCorrelationProjectionHistograms;



};
#endif