/*!
        \file                DQMHistogramPulseShape.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
*/

#ifndef __DQMHISTOGRAMPULSESHAPE_H__
#define __DQMHISTOGRAMPULSESHAPE_H__
#include "../DQMUtils/DQMHistogramBase.h"
#include "../Utils/Container.h"
#include "../Utils/DataContainer.h"

class TFile;

/*!
 * \class DQMHistogramPulseShape
 * \brief Class for PulseShape monitoring histograms
 */
class DQMHistogramPulseShape : public DQMHistogramBase
{

  public:
    /*!
     * constructor
     */
    DQMHistogramPulseShape ();

    /*!
     * destructor
     */
    ~DQMHistogramPulseShape();

    /*!
     * Book histograms
     */
    void book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill (std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process () override;

    /*!
     * Reset histogram
     */
    void reset(void) override;
    //virtual void summarizeHistos();

    void fillPulseShapePlots(uint16_t Delay, DetectorDataContainer &thePulseShapeContainer);

  private:
    DetectorDataContainer fDetectorData;

    DetectorDataContainer fDetectorChipPulseShapeHistograms;
    DetectorDataContainer fDetectorMultiGraphPulseShapeHistograms;


};
#endif