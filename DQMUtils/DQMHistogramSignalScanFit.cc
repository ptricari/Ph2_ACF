/*!
        \file                DQMHistogramSignalScanFit.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger,Tricarico Pietro 
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch, pietro.matteo.tricarico@cern.ch
 */

#include "../DQMUtils/DQMHistogramSignalScanFit.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/EmptyContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ContainerFactory.h"
#include "../RootUtils/TH1FContainer.h"
#include "../RootUtils/TH2FContainer.h"
#include "../Utils/Container.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TProfile2D.h"
#include "../Utils/GenericVariableArray.h"

//======================================================================================================================================================================================================//
// Constractor and Destructor of SignalSanFit DQM(Data Quality Monitoring) Histograms                                                                                                                   //                                               
//======================================================================================================================================================================================================//

DQMHistogramSignalScanFit::DQMHistogramSignalScanFit ()
{
}
//=====================================================================================================================================================================================================//

DQMHistogramSignalScanFit::~DQMHistogramSignalScanFit ()
{

}

//======================================================================================================================================================================================================//
// Book all Histograms of SignalSanFit Calibration                                                                                                                                                      //                                               
//======================================================================================================================================================================================================//

void DQMHistogramSignalScanFit::book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap)
{
    ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

    double fSignalScanStep = pSettingsMap["SignalScanStep"];
    fNevents = pSettingsMap["Nevents"];
    LOG (INFO) <<  "cNevents: " << +fNevents;
    
    double VCthNbins = int( (1024 / double(fSignalScanStep)) + 1 ); 
    double VCthMax = double( ( VCthNbins * fSignalScanStep ) - (double(fSignalScanStep) / 2.) );
    double VCthMin = 0. - ( double(fSignalScanStep) / 2. );
    const uint32_t NCbc = 8;



    // Book Module 2D-plot Histogram with all the channels on the x-axis, Vcth on the y-axis and #clusters on the z-axis.
    HistContainer<TH2D> hSignal("moduleSignalThresholdScan","Signal threshold vs channel ; Channel # ; Threshold [Vcth]; # of Hits",NCbc * NCHANNELS, -0.5, NCbc * NCHANNELS - 0.5, VCthNbins, VCthMin, VCthMax );
    RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorSignalHistograms,hSignal);

    // Book Module 2D-plot Histogram with cluster width on the x-axis, Vcth on y-axis, counts of certain clustersize on z-axis.
    HistContainer<TH2F> hClusterSizeVCthNClusters("moduleClusterSizePerVcth_","Cluster size vs Vcth ; Cluster size [strips] ; Threshold [Vcth] ; # clusters",127, -0.5, 140.5, VCthNbins, VCthMin, VCthMax ); /// cluster size why ? 15, -0.5, 14.5,
    RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorVCthClusterSizeHistograms,hClusterSizeVCthNClusters);

    // Book Module 1D-plot Histogram with the number of triggers per VCth.
    HistContainer<TProfile> hNumberOfTriggers("moduleTotalNumberOfTriggers","Total number of triggers received ; Threshold [Vcth] ; Number of triggers",VCthNbins, VCthMin, VCthMax);
    RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorNumberOfTriggersHistograms,hNumberOfTriggers);

    // Book Chip 1D-plot Histogram with the number of Hits Even per VCth.
    HistContainer<TH1D> hVcthHitsEven("HitsEven","Hits Even; Threshold [Vcth] ; Number of hits",VCthNbins, VCthMin, VCthMax );
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorVcthHitsEvenHistograms,hVcthHitsEven);

    // Book Chip 1D-plot Histogram with the number of Hits Odd per VCth.
    HistContainer<TH1D> hVcthHitsOdd("HitsOdd","Hits Odd; Threshold [Vcth] ; Number of hits",VCthNbins, VCthMin, VCthMax);
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorVcthHitsOddHistograms,hVcthHitsOdd);

    // Book Chip 1D-plot Histogram with the number of Clusters Even per VCth.
    HistContainer<TH1D> hVcthClustersEven("ClustersEven","Clusters Even; Threshold [Vcth] ; Number of clusters",VCthNbins, VCthMin, VCthMax);
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorVcthClustersEvenHistograms,hVcthClustersEven);

    // Book Chip 1D-plot Histogram with the number of Clusters Hits Odd per VCth.
    HistContainer<TH1D> hVcthClustersOdd("ClustersOdd","Clusters Odd; Threshold [Vcth] ; Number of clusters",VCthNbins, VCthMin, VCthMax);
    RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorVcthClustersOddHistograms,hVcthClustersOdd);
}

//======================================================================================================================================================================================================//
// SignalSanFit Calibration fill, process and reset the Histograms                                                                                                                                      //                                               
//======================================================================================================================================================================================================//

bool DQMHistogramSignalScanFit::fill(std::vector<char>& dataBuffer)
{
        return false;
}

//=====================================================================================================================================================================================================//

void DQMHistogramSignalScanFit::process()
{
    for(auto board : fDetectorSignalHistograms)
    {
        for(auto module: *board)
        {
            TCanvas *moduleSignalScanCanvas = new TCanvas(Form("moduleSignalScanCanvas%d",module->getId()),Form("Module %d Signal Scan Canvas",module->getId()),10, 0, 800, 500 );
            moduleSignalScanCanvas ->Divide(2,2);

            // Draw Signal Histogram  x = Channel# ; y = Threshold# ; z = # of Hits# ;
            moduleSignalScanCanvas->cd(1);
            TH2I *moduleSignalHistograms = fDetectorSignalHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2I>>().fTheHistogram;
            moduleSignalHistograms->DrawCopy ( "colz" );

            // Draw VCthClusterSize Histogram x = Cluster size [strips] ; y = Threshold# ; z = # clusters ;
            moduleSignalScanCanvas->cd(2);
            TH2I *moduleVCthClusterSizeHistograms = fDetectorVCthClusterSizeHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2I>>().fTheHistogram;
            moduleVCthClusterSizeHistograms->DrawCopy();

            // Draw NumberOfTriggers Histogram x = Threshold [Vcth] ; y = Number of triggers ;
            moduleSignalScanCanvas->cd(3);
            TProfile *moduleNumberOfTriggersHistograms = fDetectorNumberOfTriggersHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TProfile>>().fTheHistogram;
            moduleNumberOfTriggersHistograms->DrawCopy();

            for(auto chip: *module)
            {   
                TCanvas *chipSignalScanCanvas = new TCanvas(Form("chipSignalScanCanvas%d",chip->getId()),Form("Chip %d SignalScan Canvas",chip->getId()),10, 0, 1000, 600 );
                chipSignalScanCanvas ->Divide(2,2);

                // Draw HitsEven Histogram x = Threshold# ; y = # Hits Even strips ;
                chipSignalScanCanvas->cd(1);
                TH1I *chipVcthHitsEvenHistograms = fDetectorVcthHitsEvenHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                chipVcthHitsEvenHistograms->SetFillColor ( 4 );
                chipVcthHitsEvenHistograms->SetFillStyle ( 3001 );
                chipVcthHitsEvenHistograms->DrawCopy();

                // Draw HitsOdd Histogram x = Threshold# ; y = # Hits Odd strips ;
                chipSignalScanCanvas->cd(2);
                TH1I *chipVcthHitsOddHistograms = fDetectorVcthHitsOddHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                chipVcthHitsOddHistograms->DrawCopy();

                // Draw ClustersEven Histogram x = Threshold# ; y = # Clusters Even strips ;
                chipSignalScanCanvas->cd(3);
                TH1I *chipVcthClustersEvenHistograms = fDetectorVcthClustersEvenHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                chipVcthClustersEvenHistograms->DrawCopy();

                // Draw ClustersOdd Histogram x = Threshold# ; y = # Clusters Odd strips ;
                chipSignalScanCanvas->cd(4);
                TH1I *chipVcthClustersOddHistograms = fDetectorVcthClustersOddHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                chipVcthClustersOddHistograms->DrawCopy();

            }

        }
    }

}
//===========================================================================================================================================================================================================//

void DQMHistogramSignalScanFit::reset(void)
{

}

//============================================================================================================================================================================================================//
// Fill the single plots of SignalSanFit Calibration                                                                                                                                                          //                                               
//============================================================================================================================================================================================================//
void DQMHistogramSignalScanFit::fillChipPlots(uint16_t VCth, DetectorDataContainer &theChipSignalScanContainer)
{
    for(auto board : theChipSignalScanContainer)
    {
        for(auto module: *board)
        {               
            for(auto chip: *module)
                {
                    // Fill HitsEven Histogram x = Threshold# ; y = # Hits Even strips ;
                    TH1I *chipVcthHitsEvenHistograms = fDetectorVcthHitsEvenHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                    float HitsEven = theChipSignalScanContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[0];

                    if (HitsEven<255)
                    {chipVcthHitsEvenHistograms->Fill(VCth,HitsEven);
                    chipVcthHitsEvenHistograms->SetBinError(chipVcthHitsEvenHistograms->FindBin(VCth),sqrt(HitsEven));}
                    
                    // Fill HitsOdd Histogram x = Threshold# ; y = # Hits Odd strips ;
                    TH1I *chipVcthHitsOddHistograms = fDetectorVcthHitsOddHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                    float HitsOdd = theChipSignalScanContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[1];

                    if (HitsOdd<255)
                    {chipVcthHitsOddHistograms->Fill(VCth,HitsOdd);
                    chipVcthHitsOddHistograms->SetBinError(chipVcthHitsOddHistograms->FindBin(VCth),sqrt(HitsOdd));}

                    // Fill ClustersEven Histogram x = Threshold# ; y = # Clusters Even strips ;
                    TH1I *chipVcthClustersEvenHistograms = fDetectorVcthClustersEvenHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                    float ClustersEven = theChipSignalScanContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[2];

                    if (ClustersEven<64)
                    {chipVcthClustersEvenHistograms->Fill(VCth,ClustersEven);
                    chipVcthClustersEvenHistograms->SetBinError(chipVcthClustersEvenHistograms->FindBin(VCth),sqrt(ClustersEven));}

                    // Fill ClustersOdd Histogram x = Threshold# ; y = # Clusters Odd strips ;
                    TH1I *chipVcthClustersOddHistograms = fDetectorVcthClustersOddHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
                    float ClustersOdd = theChipSignalScanContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[3];

                    if (ClustersOdd<64)
                    {chipVcthClustersOddHistograms->Fill(VCth,ClustersOdd);
                    chipVcthClustersOddHistograms->SetBinError(chipVcthClustersOddHistograms->FindBin(VCth),sqrt(ClustersOdd));}
                }
        }
    }
}
//=======================================================================================================================================================================================================//


void DQMHistogramSignalScanFit::fillModuleSignalPlots(uint16_t VCth, DetectorDataContainer &theSignalContainer)
{
    LOG (INFO) <<  "Nevents: " << +fNevents; 
    for(auto board : theSignalContainer)
    {
        for(auto module: *board)
        {   
            //Fill Signal Histogram x = Channel# ; y = Threshold# ; z = # of Hits# 
            TH2F *moduleSignalHistograms = fDetectorSignalHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2F>>().fTheHistogram;

            for(auto chip: *module)
            {
                for ( uint32_t cId = 0; cId < NCHANNELS; cId++ )
                {
                    float signalNHits = chip->getChannel<float>(cId) ;
                    moduleSignalHistograms ->SetBinContent(NCHANNELS * chip->getIndex() + cId + 1,VCth, (float(signalNHits)/float(fNevents)) );
                }
            }

        
        }
    }
}
//=======================================================================================================================================================================================================//

void DQMHistogramSignalScanFit::fillModuleClusterSizePerVcthPlots(uint16_t VCth, DetectorDataContainer &theNOfClustersClustersSizeContainer)
{
    for(auto board : theNOfClustersClustersSizeContainer)
    {
        for(auto module: *board)
        {   
            //Fill VCthClusterSize Histogram x = Cluster size [strips] ; y = Threshold# ; z = # clusters 
            TH2I *ModuleVCthClusterSizeHistograms = fDetectorVCthClusterSizeHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2I>>().fTheHistogram;

            for ( uint32_t ClusterSize = 0; ClusterSize < 128; ClusterSize++ )
            {
                int NOfClusters = theNOfClustersClustersSizeContainer.at(board->getIndex())->at(module->getIndex())->getSummary<GenericVariableArray<128>>().fVariableArray[ClusterSize];
                if (true)//NOfClusters<100 &&  NOfClusters>0)
                {ModuleVCthClusterSizeHistograms->Fill(ClusterSize,VCth,NOfClusters);}
            
            }

        }
    }
}
//=======================================================================================================================================================================================================//

void DQMHistogramSignalScanFit::fillModuleNumberOfTriggersPlots(uint16_t VCth, DetectorDataContainer &theNumberOfTriggersContainer)
{
    for(auto board : theNumberOfTriggersContainer)
    {
        for(auto module: *board)
        {   
            //Fill NumberOfTriggers Histogram x = Threshold [Vcth] ; y = Number of triggers (TotalEvents); 
            TProfile *ModuleNumberOfTriggersHistograms = fDetectorNumberOfTriggersHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TProfile>>().fTheHistogram;
            uint32_t cTotalEvents = theNumberOfTriggersContainer.at(board->getIndex())->at(module->getIndex())->getSummary<uint32_t>();
            ModuleNumberOfTriggersHistograms->Fill(VCth,cTotalEvents);
        
        }
    }
}
//=======================================================================================================================================================================================================//
