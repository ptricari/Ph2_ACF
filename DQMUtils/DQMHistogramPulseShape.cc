/*!
        \file                DQMHistogramPulseShape.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
 */

#include "../DQMUtils/DQMHistogramPulseShape.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Utilities.h"
#include "../Utils/Occupancy.h"
#include "../Utils/EmptyContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ContainerFactory.h"
#include "../RootUtils/TH1FContainer.h"
#include "../RootUtils/TH2FContainer.h"
#include "../Utils/Container.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "../Utils/GenericVariableArray.h"

//======================================================================================================================================================================================================//
// Constractor and Destructor of PulseShape DQM(Data Quality Monitoring) Histograms                                                                                                                     //                                               
//======================================================================================================================================================================================================//

DQMHistogramPulseShape::DQMHistogramPulseShape ()
{
}
//=====================================================================================================================================================================================================//

DQMHistogramPulseShape::~DQMHistogramPulseShape ()
{

}

//======================================================================================================================================================================================================//
// Book all Histograms of PulseShape Calibration                                                                                                                                                        //                                               
//======================================================================================================================================================================================================//

void DQMHistogramPulseShape::book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap)
{
        ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

        uint16_t cMaxValue = 1023;
        uint32_t fDelayAfterPulse = 196;
        int cLow = ( fDelayAfterPulse + 3 ) * 25;
        int cHigh = ( fDelayAfterPulse + 8 ) * 25;

        // Book Module 1D-plot Histograms  x = Delay [ns] ; y = Amplitude [VCth] ; 
        HistContainer<TH2I> fPulseShape("modulePulseShapeScan","PulseShape; Delay [ns]; Amplitude [VCth]", 350, 0, cHigh-cLow, cMaxValue, 0, cMaxValue);
        RootContainerFactory::bookChipHistograms(theOutputFile,theDetectorStructure,fDetectorChipPulseShapeHistograms,fPulseShape);

        // Book Module 1D-plot Histograms  x =  ; y =  ; 
        //HistContainer<TMultiGraph> fHistBottom("cbc_pulseshape","cbc_pulseshape", ( cNBinsX ), -0.5, ( cNBinsX ) - 0.5 );
        //RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorBottomHistograms,fHistBottom);


}

//======================================================================================================================================================================================================//
// PulseShape fill, process and reset the Histograms                                                                                                                                                    //                                               
//======================================================================================================================================================================================================//

bool DQMHistogramPulseShape::fill(std::vector<char>& dataBuffer)
{
        return false;
}

//=====================================================================================================================================================================================================//

void DQMHistogramPulseShape::process()
{
    for(auto board : fDetectorChipPulseShapeHistograms)
    {
        for(auto module: *board)
        {
            for(auto chip: *module)
            {   
                TCanvas *chipPulseShape = new TCanvas(Form("chipPulseShape%d",chip->getId()),Form("Chip %d PulseShape Canvas",chip->getId()),10, 0, 1000, 600 );
                chipPulseShape ->Divide(1,2);

                // Draw PulseShape Histograms x = Delay [ns] ; y = Amplitude [VCth] ;
                chipPulseShape->cd(1);
                TH1F *chipPulseShapeistograms = fDetectorChipPulseShapeHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                chipPulseShapeistograms->DrawCopy();

                // // Draw GainElectron Histograms x = Channel# ; y = Gain (Electrons) ; 
                // chipPulseShape->cd(2);
                // TH1F *chipGainElectronsHistograms = fDetectorChipPulseShapeHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                // chipGainElectronsHistograms->DrawCopy();

            }

        }
    }

}

//=====================================================================================================================================================================================================//

void DQMHistogramPulseShape::reset(void)
{

}

//======================================================================================================================================================================================================//
// Fill the single plots of PulseShape                                                                                                                                                                  //                                               
//======================================================================================================================================================================================================//

void DQMHistogramPulseShape::fillPulseShapePlots(uint16_t Delay, DetectorDataContainer &thePulseShapeContainer)
{
    for(auto board: thePulseShapeContainer)
    {
        for(auto module: *board)
        {
            for(auto chip: *module)
            {
                // Fill GainAmp Histograms x = Delay [ns] ; y = Amplitude [VCth] ; 
                TH1F *PulseShapeAmpHistograms = fDetectorChipPulseShapeHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
                float Amplitude = thePulseShapeContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<2>>().fVariableArray[0];
                PulseShapeAmpHistograms->Fill(Delay,Amplitude);
                PulseShapeAmpHistograms->SetBinError(PulseShapeAmpHistograms->FindBin(Delay),sqrt(Amplitude));

            }
        }
    }
}

//=====================================================================================================================================================================================================//

// void DQMHistogramTPCalibration::fillChipGainPlots(uint16_t Channel, DetectorDataContainer &theGainContainer)
// {
//     for(auto board: theGainContainer)
//     {
//         for(auto module: *board)
//         {
//             for(auto chip: *module)
//             {
//                 // Fill GainAmp Histograms x = Channel# ; y = Gain (Amp) ; 
//                 TH1F *chipGainAmpHistograms = fDetectorChannelGainAmpHistograms.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<HistContainer<TH1F>>().fTheHistogram;
//                 float gainAmp = theGainContainer.at(board->getIndex())->at(module->getIndex())->at(chip->getIndex())->getSummary<GenericVariableArray<4>>().fVariableArray[0];
//                 chipGainAmpHistograms->Fill(Channel,gainAmp);
//                 chipGainAmpHistograms->SetBinError(chipGainAmpHistograms->FindBin(Channel),sqrt(gainAmp));
                
//             }
//         }
//     }
// }

//=====================================================================================================================================================================================================//