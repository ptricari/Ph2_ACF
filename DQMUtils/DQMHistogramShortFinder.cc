/*!
        \file                DQMHistogramShortFinder.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
 */

#include "../DQMUtils/DQMHistogramShortFinder.h"
#include "../Utils/ContainerStream.h"
#include "../Utils/ThresholdAndNoise.h"
#include "../Utils/Utilities.h"
#include "../Utils/Occupancy.h"
#include "../Utils/EmptyContainer.h"
#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/ContainerFactory.h"
#include "../RootUtils/TH1FContainer.h"
#include "../RootUtils/TH2FContainer.h"
#include "../Utils/Container.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "../Utils/GenericVariableArray.h"

//=======================================================================================================================================================================================================//
// Constractor and Destructor of ShortFinder DQM(Data Quality Monitoring) Histograms                                                                                                                     //                                               
//=======================================================================================================================================================================================================//

DQMHistogramShortFinder::DQMHistogramShortFinder ()
{
}
//=======================================================================================================================================================================================================//

DQMHistogramShortFinder::~DQMHistogramShortFinder ()
{

}

//=======================================================================================================================================================================================================//
// Book all Histograms of ShortFinder Calibration                                                                                                                                                        //                                               
//=======================================================================================================================================================================================================//

void DQMHistogramShortFinder::book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap)
{
        uint32_t fNCbc = 16;
        uint32_t cNBinsX = (fNCbc * 254) / 2;

        ContainerFactory::copyStructure(theDetectorStructure, fDetectorData);

        // Book Module 1D-plot Histograms  x = Pad Number ; y = Occupancy [%] ; 
        HistContainer<TH1D> fHistTop("moduleSignalThresholdScan","Front Pad Channels; Pad Number; Occupancy [%]", ( cNBinsX ), -0.5, ( cNBinsX ) - 0.5  );
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorTopHistograms,fHistTop);

        // Book Module 1D-plot Histograms  x = Pad Number ; y = Occupancy [%] ; 
        HistContainer<TH1D> fHistBottom("moduleSignalThresholdScan","Back Pad Channels; Pad Number; Occupancy [%]", ( cNBinsX ), -0.5, ( cNBinsX ) - 0.5 );
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorBottomHistograms,fHistBottom);

        // HistContainer<TH1D> fHistTopMerged("moduleSignalThresholdScan","Front Pad Channels; Pad Number; Occupancy [%]", ( cNBinsX ) , -0.5, ( cNBinsX ) - 0.5 );
        // RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorTopMergedHistograms,fHistTopMerged);

        // HistContainer<TH1D> fHistBottomMerged("moduleSignalThresholdScan","Back Pad Channels; Pad Number; Occupancy [%]", ( cNBinsX ) , -0.5, ( cNBinsX ) - 0.5  );
        // RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorBottomMergedHistograms,fHistBottomMerged);


        // HistContainer<TH1D> fHistShortBackground("moduleSignalThresholdScan","",( cNBinsX ) , -0.5, ( cNBinsX ));// - 1.5 , 2  , -0.5 , 2.0 - 0.5 );
        // RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorShortBackgroundHistograms,fHistShortBackground);

        // Book Module 1D-plot Histograms  x = strip# ; y = Binary Variable Functional/Shorted ;  
        HistContainer<TH1D> fHistShortsTop("moduleSignalThresholdScan","Shorts on Front Pad Channels", ( cNBinsX ) , -0.5, ( cNBinsX ) - 0.5  );
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorShortsTopHistograms,fHistShortsTop);

        // Book Module 1D-plot Histograms  x = strip# ; y = Binary Variable Functional/Shorted ;   
        HistContainer<TH1D> fHistShortsBottom("moduleSignalThresholdScan","Shorts on Back Pad Channels", ( cNBinsX ) , -0.5, ( cNBinsX ) - 0.5 );
        RootContainerFactory::bookModuleHistograms(theOutputFile,theDetectorStructure,fDetectorShortsBottomHistograms,fHistShortsBottom);

}

//=======================================================================================================================================================================================================//
// ShortFinder fill, process and reset the Histograms                                                                                                                                                    //                                               
//=======================================================================================================================================================================================================//

bool DQMHistogramShortFinder::fill(std::vector<char>& dataBuffer)
{
        return false;
}

//=====================================================================================================================================================================================================//
void DQMHistogramShortFinder::process()
{
    for(auto board : fDetectorTopHistograms)
    {
        for(auto module: *board)
        {
            TCanvas *moduleOccupancyCanvas = new TCanvas(Form("moduleOccupanyCanvas%d",module->getId()),Form("Module %d Signal Scan Canvas Occupancy ",module->getId()),10, 0, 800, 500 );
            moduleOccupancyCanvas ->Divide(2,1);

            // Draw Top Histograms  x = Pad Number ; y = Occupancy [%] ; 
            moduleOccupancyCanvas->cd(1);
            TH1I *moduleTopHistograms = fDetectorTopHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            moduleTopHistograms->DrawCopy ();

            // Draw Bottom Histograms  x = Pad Number ; y = Occupancy [%] ; 
            moduleOccupancyCanvas->cd(2);
            TH1I *moduleBottomHistograms = fDetectorBottomHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            moduleBottomHistograms->DrawCopy();


            // moduleOccupanyCanvas->cd(3);
            // TH1I *moduleTopMergedHistograms = fDetectorTopMergedHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            // moduleTopMergedHistograms->DrawCopy();

            // moduleOccupanyCanvas->cd(3);
            // TH1I *moduleBottomMergeHistograms = fDetectorBottomMergedHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            // moduleBottomMergeHistograms->DrawCopy();
            
            //==================================================================================================================================================================================//

            TCanvas *moduleSignalScanCanvas = new TCanvas(Form("moduleSignalScanCanvas%d",module->getId()),Form("Module %d Signal Scan Canvas",module->getId()),10, 0, 1200, 500 );
            moduleSignalScanCanvas ->Divide(2,1);

            // Draw ShortsTop Histograms  x = strip# ; y = Binary Variable Functional/Shorted ;
            moduleSignalScanCanvas->cd(1);
            TH2I *moduleShortsTopHistograms = fDetectorShortsTopHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH2I>>().fTheHistogram;
            moduleShortsTopHistograms->SetFillColor ( 4 );
            moduleShortsTopHistograms->SetFillStyle ( 3001 );

            //moduleShortsTopHistograms->GetYaxis()->SetRangeUser (-0.5, 1.5);
            moduleShortsTopHistograms->GetYaxis()->SetRangeUser (0, 3);
            moduleShortsTopHistograms->GetYaxis()->Set(3,0.5, 3.5);
            moduleShortsTopHistograms->GetXaxis()->SetTitle ("Pad Number");
            moduleShortsTopHistograms->GetYaxis()->SetTitleOffset (1.6);
            moduleShortsTopHistograms->GetYaxis()->SetBinLabel (1, "Functional" );
            moduleShortsTopHistograms->GetYaxis()->SetBinLabel (2, "Shorted" );
            moduleShortsTopHistograms->DrawCopy();

            // Draw ShortsBottom Histograms  x = strip# ; y = Binary Variable Functional/Shorted ;
            moduleSignalScanCanvas->cd(2);
            TH1I *moduleShortsBottomHistograms = fDetectorShortsBottomHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            moduleShortsBottomHistograms->SetFillColor ( 4 );
            moduleShortsBottomHistograms->SetFillStyle ( 3001 );
            moduleShortsBottomHistograms->GetXaxis()->SetTitle ("Pad Number");
            moduleShortsBottomHistograms->GetYaxis()->SetRangeUser (0, 3);
            moduleShortsBottomHistograms->GetYaxis()->Set(3,0.5, 3.5);
            moduleShortsBottomHistograms->GetYaxis()->SetTitleOffset (1.6);
            moduleShortsBottomHistograms->GetYaxis()->SetBinLabel (1, "Functional" );
            moduleShortsBottomHistograms->GetYaxis()->SetBinLabel (2, "Shorted" );
            moduleShortsBottomHistograms->DrawCopy();




        }
    }

}

//=====================================================================================================================================================================================================//

void DQMHistogramShortFinder::reset(void)
{

}

//=======================================================================================================================================================================================================//
// Fill the single plots of ShortFinder                                                                                                                                                                  //                                               
//=======================================================================================================================================================================================================//

void DQMHistogramShortFinder::fillModuleOccupancyPlots(uint16_t padNumber, DetectorDataContainer &theOccupancyContainer)
{
    for(auto board : theOccupancyContainer)
    {
        for(auto module: *board)
        {   
            //Fill Top Histograms  x = Pad Number ; y = Occupancy [%] ; 
            TH1I *ModuleTopHistograms = fDetectorTopHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            float occupancyTop = theOccupancyContainer.at(board->getIndex())->at(module->getIndex())->getSummary<GenericVariableArray<2>>().fVariableArray[0];
            ModuleTopHistograms->Fill(padNumber,occupancyTop);

            //Fill Bottom Histograms  x = Pad Number ; y = Occupancy [%] ;
            TH1I *ModuleBottomHistograms = fDetectorBottomHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            float occupancyBottom = theOccupancyContainer.at(board->getIndex())->at(module->getIndex())->getSummary<GenericVariableArray<2>>().fVariableArray[1];
            ModuleBottomHistograms->Fill(padNumber,occupancyBottom);
        
        }
    }
}
//=======================================================================================================================================================================================================//

void DQMHistogramShortFinder::fillShortsPlots(uint16_t padNumber, DetectorDataContainer &theShortContainer)
{
    for(auto board : theShortContainer)
    {
        for(auto module: *board)
        {   
            // TH1I *ModuleShortBackgroundHistograms = fDetectorShortBackgroundHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            // float cTotalEvents = theShortContainer.at(board->getIndex())->at(module->getIndex())->getSummary<GenericVariableArray<3>>().fVariableArray[0];
            // ModuleShortBackgroundHistograms->Fill(padNumber,cTotalEvents);
            
            //Fill ShortsTop Histograms  x = strip# ; y = Binary Variable Functional/Shorted ; 
            TH1I *ModuleShortsTopHistograms = fDetectorShortsTopHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            float cTotalEvents1 = theShortContainer.at(board->getIndex())->at(module->getIndex())->getSummary<GenericVariableArray<3>>().fVariableArray[1];
            ModuleShortsTopHistograms->Fill(padNumber,cTotalEvents1);
            
            //Fill ShortsBottom Histograms  x = strip# ; y = Binary Variable Functional/Shorted ;
            TH1I *ModuleShortsBottomHistograms = fDetectorShortsBottomHistograms.at(board->getIndex())->at(module->getIndex())->getSummary<HistContainer<TH1I>>().fTheHistogram;
            float cTotalEvents2 = theShortContainer.at(board->getIndex())->at(module->getIndex())->getSummary<GenericVariableArray<3>>().fVariableArray[2];
            ModuleShortsBottomHistograms->Fill(padNumber,cTotalEvents2);
        
        }
    }
}
//=======================================================================================================================================================================================================//


