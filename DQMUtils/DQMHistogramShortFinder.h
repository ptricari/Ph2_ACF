/*!
        \file                DQMHistogramShortFinder.h
        \brief               base class to create and fill monitoring histograms
        \author              Fabio Ravera, Lorenzo Uplegger, Tricarico Pietro
        \version             1.0
        \date                6/5/19
        Support :            mail to : fabio.ravera@cern.ch
*/

#ifndef __DQMHISTOGRAMSHORTFINDER_H__
#define __DQMHISTOGRAMSHORTFINDER_H__
#include "../DQMUtils/DQMHistogramBase.h"
#include "../Utils/Container.h"
#include "../Utils/DataContainer.h"

class TFile;
/*!
 * \class DQMHistogramShortFinder
 * \brief Class for ShortFinder monitoring histograms
 */
class DQMHistogramShortFinder : public DQMHistogramBase
{

  public:
    /*!
     * constructor
     */
    DQMHistogramShortFinder ();

    /*!
     * destructor
     */
    ~DQMHistogramShortFinder();

    /*!
     * Book histograms
     */
    void book(TFile *theOutputFile, const DetectorContainer &theDetectorStructure, std::map<std::string, double> pSettingsMap) override;

    /*!
     * Fill histogram
     */
    bool fill (std::vector<char>& dataBuffer) override;

    /*!
     * Save histogram
     */
    void process () override;

    /*!
     * Reset histogram
     */
    void reset(void) override;
    //virtual void summarizeHistos();

    void fillModuleOccupancyPlots(uint16_t Pad, DetectorDataContainer &theOccupancyContainer);
    void fillShortsPlots(uint16_t Pad, DetectorDataContainer &theShortContainer);
   

  private:
    DetectorDataContainer fDetectorData;

    DetectorDataContainer fDetectorTopHistograms;
    DetectorDataContainer fDetectorBottomHistograms;
    DetectorDataContainer fDetectorTopMergedHistograms;
    DetectorDataContainer fDetectorBottomMergedHistograms;

    DetectorDataContainer fDetectorShortBackgroundHistograms;
    DetectorDataContainer fDetectorShortsTopHistograms;
    DetectorDataContainer fDetectorShortsBottomHistograms;



};
#endif