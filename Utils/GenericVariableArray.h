/*

        \file                          NOfClustersClusterSize.h
        \brief                         Generic GenericVariableArray for DAQ SignalScanFit
        \author                        Fabio Ravera, Lorenzo Uplegger,Tricarico Pietro
        \version                       1.0
        \date                          08/04/19
        Support :                      mail to : fabio.ravera@cern.ch

 */

#ifndef __GENERICVARIABLEARRAY_H__
#define __GENERICVARIABLEARRAY_H__

#include <iostream>

template<uint8_t N>
class GenericVariableArray 
{
public:
	GenericVariableArray()
	{;}
	~GenericVariableArray(){;}

    float fVariableArray[N];
};

#endif