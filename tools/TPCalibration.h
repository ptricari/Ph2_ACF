#ifndef TPCalibration_h__
#define TPCalibration_h__

#include "Tool.h"
#include "../Utils/Visitor.h"
#include "../Utils/Utilities.h"
#include "../Utils/CommonVisitors.h"
#include "TString.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TH1F.h"
#include "TF1.h"
#include "../tools/PedeNoise.h"
//#include "../tools/tmp/OldPedenoise.h"

#ifdef __USE_ROOT__
  #include "../DQMUtils/DQMHistogramTPCalibration.h"
#endif

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;


/*!
 * \class TPCalibration
 * \CMS Ph2_ACF test pulse calibration class to perform the calibration of threshold gain calibration
 */



class TPCalibration : public PedeNoise
{
private: //attributes
  int fStartAmp;
  int fEndAmp;
  int fStepsize;
  int fTPCount;

public: //methods
  TPCalibration();
  ~TPCalibration();


  void Init(int pStartAmp, int pEndAmp, int pStepsize);
  void InitNew();
  void RunCalibration();
  void SaveResults();
  float ConvertAmpToElectrons(float pTPAmp, bool pOffset);
  void writeObjects();
  
  void Start(int currentRun) override;
  void Stop() override;
  void ConfigureCalibration() override;
  void Pause() override;
  void Resume() override;

private: //methods
  void FillHistograms(int pTPAmp);
  void FitCorrelations();


  #ifdef __USE_ROOT__
     DQMHistogramTPCalibration fDQMHistogramTPCalibration;
   #endif
};

#endif
